﻿public class Cow
{
    private string name;

    private float weight;

    private int age;

    private int milkProduction;

    public Cow(string name, float weight)
    {
        this.name = name;
        this.weight = weight;
    }

    public Cow(string name)
    {
        this.name = name;
    }
    
    public int MilkProduction
    {
        get
        {
            return milkProduction;
        }
        set
        {
            if (value >= 0)

                milkProduction = value;

            else

                throw new ArgumentException("Milk Production cannot be less than zero.");
        }
    }

    public int MilkProductionAuto { get; set; }

    public string Name => name;

    public void MakeSound()
    {
        Console.WriteLine($"My name is {name}");
    }

    public float GetWeight()
    {
        return weight;
    }
}